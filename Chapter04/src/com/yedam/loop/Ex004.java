package com.yedam.loop;

public class Ex004 {
	public static void main(String[] args) {
		while(true) {
			int num = (int)(Math.random()*6) +1;
			System.out.println(num);
			if(num ==6) {
				break;
			}
		}
		System.out.println("end of prog");
		//이중for문에서 안쪽 for문에서 break를 사용하면 안쪽 for문만 종료됨
		
		for(int i =0; i<10; i++) {
			for(int j =0; j<=10; j++) {
				if(i+j == 4) {
					System.out.println("i+j="+(i+j));
					break;
					//System.out.println("안나갔다."); //논리적 오류
				}
			}
		}
		
		
		Outter : for(char upper ='A'; upper <= 'z'; upper++) {
			for(char lower='a'; lower <= 'z'; lower++) {
				System.out.println(upper + "-" + lower);
				if(lower == 'g') {
					break Outter;
				}
			}
		}
		
		for(int i=0; i<=10; i++) {
			if(i%2 == 0) {
				continue; //짝수일 경우 continue, 위로 올라감
			}
			System.out.println(i); //짝수일 경우 i에 저장, 출력
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		System.out.println("end of prog");
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}
