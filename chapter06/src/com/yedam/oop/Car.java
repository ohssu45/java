package com.yedam.oop;

public class Car {
	//필드
	String model;
	int speed;
	//생성자
	Car(String model){
		this.model= model;
	}
	
	//메소드
	void stepSpeed(int speed) {
		this.speed = speed;
	}
	
	void run() {
		//반복문을 만듭니다.
		//조건 : i => 10부터 시작
		//		i => 50보다 작을 때까지 반복
		//		반복문을 한번 실행하고 나면, i의 값을 10씩 증가시켜주세요.
		for(int i = 10; i<50; i+=10) {
			this.stepSpeed(i);
			System.out.println(this.model + "가 달립니다.(시속 : )" +this.speed+"km");
		}
	}
}
	
	
	
	
	
	
	
	
	
	
	
	

