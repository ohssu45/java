package com.yedam.oop;

public class Student1128 {

	//필드
	private String stdName;
	private String stdMajor;
	private String stdgrade;
	private int programing;
	private int dateBase;
	private int OS;
	
	
	
	//생성자
	//필드 private => setter, getter를 사용하기 위해
	//클래스를 통한 객체를 생성할 때 첫번째로 수행하는 일들을 모아두는 곳
	//필드에 대한 데이터를 객체를 생성할 때 초기화 할 예정이라면
	//생성자에게 this 키워드를 활용해서 필드 초기화 하면 됨.

	
	
	//메서드
	public String getStdName() {
		return stdName;
	}
	public void setStdName(String stdName) {
		this.stdName = stdName;
	}
	public String getStdMajor() {
		return stdMajor;
	}
	public void setStdMajor(String stdMajor) {
		this.stdMajor = stdMajor;
	}
	public String getStdgrade() {
		return stdgrade;
	}
	public void setStdgrade(String stdgrade) {
		this.stdgrade = stdgrade;
	}
	public int getPrograming() {
		
		return programing;
	}
	public void setPrograming(int programing) {
		//프로그래밍 언어 점수가 0보다 작은 점수가 들어올 경우
				//
				if(programing <= 0) {
					this.programing = 0;
				}
				this.programing = programing;
		this.programing = programing;
	}
	public int getDateBase() {
		return dateBase;
	}
	public void setDateBase(int dateBase) {
		this.dateBase = dateBase;
	}
	public int getOS() {
		return OS;
	}
	public void setOS(int oS) {
		OS = oS;
	}
}
