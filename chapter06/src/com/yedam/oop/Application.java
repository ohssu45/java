package com.yedam.oop;

public class Application {
	public static void main(String[] args) {
		//객체만들기, 물건만들기
		//SmartPhone 클래스(설계도를 토대로 iphone14Pro만들기)
		//정보 추가
		SmartPhone iphone14Pro = new SmartPhone("Apple", "iphohe14Pro", 500); //물건을 만듬과 동시에 값을 넣음
//		iphone14Pro.maker = "Apple";
//		iphone14Pro.name = "iphon14Pro";
//		iphone14Pro.price = 100000;
		//물건을 만들고 난 후 값을 넣음
		// . : 설계도 정보 가져오기//정보기입
		iphone14Pro.price = 500; //정보 변경가능
		iphone14Pro.call();
		iphone14Pro.hangUp();
		//기능
		
		//필드 정보 읽기
		System.out.println(iphone14Pro.maker);
		System.out.println(iphone14Pro.name);
		System.out.println(iphone14Pro.price);
		
		//SmartPhone 클래스(설계도)
		
		SmartPhone zfilp4 = new SmartPhone();
		zfilp4.maker = "samsung";
		zfilp4.name = "zfilp4";
		zfilp4.price = 10000;
		
		zfilp4.call();
		zfilp4.hangUp();
		
		// System.out.println(iphone14.maker); //apple
		// 설계도의 값을 가져와서 물건에 저장하기 때문에 samsung이 아닌 apple이 출력됨

	
		SmartPhone sony = new SmartPhone();
		//리턴 타입이 없는 메소드
//		int a = sony.getInf(0);
		
		//리턴타입이 int인 메소드
		int b = sony.getInfo("int");
		
		//리턴 타입이 String[]인 메소드
		String[] temp = sony.getInfo(args);
		
	
		
	
	
	
	
	
	
	
	
	
	
	
	
	}
}
