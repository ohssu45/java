package com.yedam.oop;

import com.yedam.access.Access;

public class Application5 {
	public static void main(String[] args) {
		Access access = new Access();
		
		//public
		access.free = "free";
		
		//protected
		//access.parent = "parent";
		//같은 패키지가 아니기 때문에 오류가 남
		
		//private
		//access.privacy = "privacy";
		//해당 클래스 내부에서만 사용할 수 있기 때문에 오류 남
		
		//default
		//access.basic = "basic";
		//같은 패키지가 아니기 때문에 사용불가
		
		access.free();
		//access.privacy();
		
		//=======================
//		Singleton obj1 = Singleton.getInstance();
//		Singleton obj2 = Singleton.getInstance();
		
//		if(obj1 == obj2) {
//			System.out.println("같은 싱글톤 객체입니다.");
//		}else {
//			System.out.println("");
//		}
//		
		
	}
}
