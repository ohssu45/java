package com.yedam.oop;

public class SmartPhone {
	
		//필드
		//객체의 정보를 저장
		String name;
		String maker;
		int price;
		
		
		//생성자
		
		//생성자(클래스 이름과 똑같이 부여해서 만듦)
		//자바에서 생성자가 클래스 내부에 "하나도 없을 때" 알아서 기본 
		//생성자를 만들고 객체를 생성
		//public SmartPhone(String name, String maker, int price){
		//	this.name = name; //this = String name을 지칭함
		//	this.maker = maker;
		//	this.price = price;
		//}//필드의 데이터를 초기화 하면서 가져온 매개변수를 필드에 입력할 때 
		
		
		public SmartPhone() {
			
		}
		public SmartPhone(String name) {
			//객체를 만들 때 내가 원하는 행동 또는 데이터 저장 등등
			//할 때 여기에 내용을 구현
		}
		public SmartPhone (int price) {
			//위와 매개변수의 형식이 다르기때문에 오류나지 않음
		}
		public SmartPhone(String name, int price) {
			
		}
		public SmartPhone(String name, String maker, int price) {
			
		}
		
		//메소드
		//객체의 기능을 정의
		void call() {
			System.out.println(name + "전화를 겁니다.");	
		} 
		void hangUp() {
			System.out.println(name + "전화를 끊니다.");
		}
		
		//1)리턴 타입이 없는 경우 : void
		void getInf(int no) {
			System.out.println("매개변수 : " +no);
		}
		
		//2)리턴 타입이 있는 경우
		//1.기본 타입 : int, double, long,,,,
		//2. 참조 타입: String, 배열, 클리스,,,,
		//2-1)리턴 타입이 기본 타입일 경우
		int getInfo(String temp) { //int 타입이기 때문에 초기값 :0
			//가져온 int 값은 temp에저장
			return 0; //return을 넣지 않으면 오류
			//값을 가져가기 위해선 반드시 return을 써줘야함
		}
	
		//2-2)리턴 타입이 참조 타입일 경우
		String[] getInfo(String[] temp) {
			
			return temp; // String 타입이기 때문에  => temp로 맞춰줌
		}
		
		
		
		
		
		
		
}
