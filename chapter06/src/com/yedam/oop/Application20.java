package com.yedam.oop;

import java.util.Scanner;

import com.yedam.access.Access;

//import com.yedam.access.Access; -직접입력

public class Application20 {
	Access ac = new Access();
	Scanner sc = new Scanner(System.in);
	//ctrl + shift - o
	
	
//================================	
	
	int speed;
	
	void run() {
		System.out.println(speed + "으로 달립니다.");
	}
	
	//메소드 영역에 등록된 친구들
	public static void main1(String[] args) {
		
	
		//int speed2 = speed; 오류
		
		//run(); => 사용할려면 앞에 static을 붙여야함
		Application20 app = new Application20(); //내가 있는 클래스를 객체로 만들어줌
		app.speed = 5;
		app.run();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static void main(String[] args) {
		Car myCar = new Car("포르쉐");
		Car yourCar = new Car("벤츠");
		
		myCar.run();
		yourCar.run();
		
		//정적필드, 메소드를 부르는 방법
		//정적 멤버가 속해 있는 클리스명, 필드 또는 메소드명
		//pi 라는 객체를 따로 만들지 않아도 바로 사용 가능
		
		//1)정적 필드 가져오는 방법
		double piRatio = Calculator.pi;
		System.out.println(piRatio);
		//2)정적 메소드 가져오는 방법
		int result = Calculator.plus(5, 6);
		System.out.println(result);
		
		//1) ☆★☆★모든 클래스에서 가져와서 사용할 수 있음 => 공유의 개념
		//2) ☆★☆★너무 남용해서 사용하면 메모리 누수(부족)현상이 발생할 수 있음
		//	메소드 영역에 저장
		//	static 영역 안에서 사용가능
		//	객체를 만들어서 사용
		//3) 주의 할 점★☆
		// 정적 메소드에서 외부에 정의한 필드를 사용하려면,
		// static이 붙은 필드 또는 메소드만 사용가능
		// static 붙이지 않고 사용하고 싶다면,
		// 해당 필드 메소드가 속해 있는 클래스를 인스터스화 해서
		// 인스턴스 필드와 인스터스메소드를 dot(.)연산자를 통해 가져와서 사용
		
	//==============================================
		
		Person p1 =new Person("123123-123456", "김또치");
		
		System.out.println(p1.nation);
		System.out.println(p1.ssn);
		System.out.println(p1.name);
		
		//p1.nation = "USA"; =>오류 final은 변경 불가 
		
		//원넓이
		System.out.println(5*5*ConstantNo.PI);
		
	}
}
