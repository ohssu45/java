package com.yedam.oop;

public class Application2 {
	public static void main(String[] args) {
		Calculator cl = new Calculator();
		
		int sumResult = cl.sum(1,  2);
		
		double subResult = cl.sub(10,  20);
		
		System.out.println(sumResult);
		System.out.println(subResult);
		//String temp = cl.result("메소드 연습");
		//System.out.println(temp);
		//System.out.println(cl.result("메소드 연습"));
		//3단 논법을 활용해서 위의 두 줄을 합쳐서 한 줄로 표현
		cl.result("메소드 연습");
		
		
		//cl.
		//.연산자를 사용해서 메소드오버로딩 리스트 중 하나를 선택하여 사용 
		
		
		Computer myCom = new Computer();
		int result = myCom.sum(1,2,3);
		System.out.println(result);
		result = myCom.sum(1,2,3,4,5,6);
		System.out.println(result);
		
		
		
		
		
		
	}
}
