package com.yedam.oop;

public class Application10 {
	public static void main(String[] args) {
		Student std1 = new Student("김또치", "예담고", "221124");//객체를 만듦과 동시에 
		Student std2 = new Student("이또치", "예담고", "221125");//데이탈,ㄹ 낳ㅇ,ㅁ
		Student std3 = new Student("박또치", "예담고", "221126");
		Student std4 = new Student("최또치", "예담고", "221127");
		Student std5 = new Student("정또치", "예담고", "221128");
		Student std6 = new Student("장또치", "예담고", "221129");
		//배열에 넣어서 써보기
		//int[] intAry = new int[6];
		//intAry[0] =1; 1이란 숫자를 넣을 수 있는 건 int 타입의 배열을 만들었기 때문
		Student[] stdAry = new Student[6];
		stdAry[0]=std1; //학생 한명씩 들어가 있음
		stdAry[1]=std2;// stdAry 1번 주소에는 std2번의 값이 들어가 있음
		stdAry[2]=std3;
		stdAry[3]=std4;
		stdAry[4]=std5;
		stdAry[5]=std6;
		//반복문을 통해 객체를 다루기 쉬워짐
		
		for(int i=0; i<stdAry.length; i++) {
			stdAry[i].kor= 50;//데이터를 넣기
			stdAry[i].math = 60;
			stdAry[i].eng = 100;
		}
		 //std1.getInfo();
		
		
		
		
		
	}
}
