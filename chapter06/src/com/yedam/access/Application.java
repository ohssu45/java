package com.yedam.access;

public class Application {
	public static void main(String[] args) {
		Access access = new Access();
		
		//public
		access.free ="free";
		
		//protected
		access.parent = "parent";
		
		//private
		//access.privacy = "privacy";
		//같은 클래스에서만 사용할 수 있기 때문에 오류
				
		//default
		access.basic = "basic";
	}
}
