package com.yedam.access;

public class Access {
	
	/*
	 * public 어디서든 누구나 접근 가능
	 * protected 상속 받은 상태에서 부모-자식간에 사용(패키지가 달라도 사용 가능)
	 * 			패키지가 다르면 사용 못함 => 같은 패키지에서만 사용가능
	 * default 패키지가 다르면, 사용 못함 => 같은 패키지에서만 사용가능
	 * private 내가 속한 클래스에서만 사용가능 
	 * 
	 */
	//접근제한자 -> 이름을 지어서 사용(변수, 클래스, 메소드 등등)
	//필드
	public String free;
	protected String parent;
	private String privacy;
	String basic;
	
	//생성자
	public Access() { //객체를 만들 때 접근이 가능하기 위해선 public
		
	}
	private Access(String privacy) { //이생성자는 사용할 수 없음
		this.privacy = privacy;
	}
	
	
	
	
	
	
	//메소드
	//public void run() {
	//	System.out.println("달립니다.");
	//}
	public void free() {
		System.out.println("접근이 가능합니다.");
		privacy();//public 안에 넣으면 사용 가능 ex)회사의 특허, 기밀사항을 외부로부터 보호, 실행만 시킴
		
	}
	private void privacy() {
		System.out.println("접근이 불가능합니다.");
	}
}
