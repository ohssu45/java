package com.yedam.access;

public class Singleton {

	//정적 필드
	//단 하나의 객체 생성 - 아무도 접근 할 수 없는 내부객체를 만듦
	private static Singleton singleton = new Singleton();
	//생성자
	private Singleton() {
		
	}
	//정적 메소드
	public static Singleton getInstance() {
		return singleton;
	}
}
