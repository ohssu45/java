package com.yedam.access;

public class Member {

	//필드
	private String id;
	private String pw;
	private String name;
	private int age;
	//생성자
	
	//메소드
	//setter, getter -> 데이터의 무결성을 지키기 위해서
	//데이터를 넣기
	public void setAge(int age) {
		if(age < 0) { //말이 안되는 데이터가 들어가는 것을 막음
			System.out.println("잘못된 나이입니다.");
			this.age = 0;
			//return; //return; -> return;을 만나면 하던 것을 멈추고 메소드를
					//종료 한 후, 메소드 호출한 곳으로 이동.
		}else {
			this.age = age;

		}
		System.out.println("return 적용 안됨.");
	}
	
	public int getAge() { //데이터를 전달하기 때문에 return과 int 필수
		//미국 나이와 한국 나이는 한살 차이 나므로 
		//아래 내용을 실행함
		age = age +1;
		return age;
	}

	public String getId() {
		return id;
	}
	//원하는 조건에 충족하는지 하지 않는지
	public void setId(String id) {
		if(id.length() <= 8) {
			System.out.println("8글자보다 부족합니다. 다시 입력해주세요");
			return;
		}
		this.id = id;
	}

	public String getPw() {
		return pw;
	}

	public void setPw(String pw) {
		this.pw = pw;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}