package homework;

public class EmpDept extends Employee {
	
	//필드
	String department;
	
	//생성자 =>부모클래스의 객체를 만들 수 있도록 부모클래스의 생성자를 호출, 생성(상속) =>super생성
	//부서 필드를 생성자를 이용하여 값을 초기화
	public EmpDept(String name, int salary, String department){
		super (name, salary);
		this.department = department;
	}
	
	//메서드
	//추가된 필드의 getter를 정의한다.
	public String getDepartment() {
		return department;
	}
	
	//Employee 클래스의 메소드를 오버라이딩한다
	// public void getInformation() : 이름과 연봉, 부서를 출력하는 기능
	@Override
	public void getInformation() {
		System.out.println("부서"+department);
		super.getInformation(); //부모의 getInformation()을 재사용 + 추가
	}
	//public void print() : "수퍼클래스\n서브클래스"란 문구를 출력하는 기능
	@Override
	public void print() {
		super.print();
		System.out.println("수퍼클래스\n서브클래스");
	}
	
	

	
	

	
	
	
}
