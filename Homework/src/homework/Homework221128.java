package homework;

public class Homework221128 {
		public static void main(String[] args) {
	// [1번 문제]
		// 369게임
		// 3, 6, 9가 포함 되어있으면 짝으로 표기한다.
		// 아래 내용처럼 출력하면 되고, 출력 범위는 1~50으로 한다.
		// ※ 사칙연산을 활용해서 풀어볼 것.
		// 출력 예시) 1 2 짝 4 5 짝 7 8 짝 10 11 12 짝 14 15 짝 17 18 짝...
			
//			for(int i=1; i<=50; i++) {
//				if(i%10==3 || i%10==6 || i%10==9) {
//					System.out.println("짝");
//				}else if(i/10==3 || i/10==6 || i/10==9) {
//					System.out.println("짝");
//				}else if(0<=i && i<=50) {
//					System.out.println(i);
//				}			
//			}		
						
	// [2번 문제]
	// answer 배열에 담긴 데이터를 읽고 각 숫자마다 개수만큼'*'를 찍는다.
	// 아래 빈 영역에 코드를 입력하여 프로그램을 완성하여라
	// ※ 배열의 각 인덱스(각 방)와 숫자의 연관성을 지으면서 풀어 볼 것.
	// 출력 예시) 1 : 3개, 2 : 2개, 3 : 2개, 4 : 4개					
						
			
			int[] answer = { 1, 4, 4, 3, 1, 4, 4, 2, 1, 3, 2 };
			//int[] counter = new int[4];
			int i =0;
			for (i = 0; i < answer.length; i++) {
				System.out.println();
				for(int j =0; j < answer[i]; j++) {
					System.out.print('*');
				}
			}
			//배열값의 중첩되는 수 
			System.out.println();
			int [] counter = new int[4];
			for(i=0; i<answer.length; i++) {
				if(answer[i]==1) {
					counter[0] += 1;
				}else if(answer[i]==2){
					counter[1] += 1;
				}else if(answer[i]==3) {
					counter[2] += 1;
				}else if(answer[i]==4) {
					counter[3] += 1;
				}
			System.out.println("1 :" + counter[i]+"개");
			System.out.println("2 :" + counter[i]+"개");
			System.out.println("3 :" + counter[i]+"개");
			System.out.println("4 :" + counter[i]+"개");
				}
				
			
							
	// [3번 문제]
	// 거스름돈 갯수 파악
	// 큰 금액의 돈을 입력 후, 동전으로 바꾸었을 때 몇개의 동전이 필요한지
	// 배열을 활용해서 구현해 본다.
				
					// 큰 금액부터 동전을 우선적으로 거슬러 줘야한다.
//					int[] coinUnit = new int[4];
//			
//					int money = 2680;
//					System.out.println("money=" + money);
//			
//					for (int i = 0; i < coinUnit.length; i++) {
//			
//						
//			
//					}
//				}
	// ※ 2번 문제와 비슷하게 각 인덱스(각 방)와 동전의 연관성을 지으면서 풀어 볼 것.
	// 출력 예시) 500원 : 5개, 100원 : 1개, 50원 : 1개, 10원 : 3개					
						
						
						
						
						
						
				
		}
	}

