package homework;

import java.util.Scanner;

public class HomeWork99 {
	public static void main(String[] args) {
		
		//문제분석
		//1. 메뉴는 다음과 같이 구성하세요
		//while(){
		//	메뉴 출력
		//  메뉴 진입할 수 있도록 조건문 통해서 구별해 주면 된다
		//	if문과 switch문
		//	Scanner sc = new Scanner(System.in);
		
		//2. 입력한 상품 수만큼 상품명과 해당 가격을 입력받아 가격을 입력 받을 수 있도록 구현
		//	객체를 여러개 넣을 수 있는 배열을 만듦
		//	입력할 때 마다 배열의 크기가 바뀜 => 고정으로 배열의 크기를 잡아주면 안됨
		//	Scanner 사용
		//  상품 클래스를 만들 때 상품명과 해당가격이 들어갈 수 있는 필드를 만들어야함
		//	입력->2번에서 만든 배열을 활용, 배열의 크기만큼 반복문을 실행함
		
		//3. 상품별 가격을 출력하세요
		//	출력 : 저장한 값을 꺼내와서 출력, 입력의 반대
		//	반복문을 활용(배열의 크기만큼) => 각 방에 있는 객체를 하나씩 꺼내옴
		//	객체가 가지고 있는 필드(정보를 담은 변수)에 맞는 출력 예시를 만듦
		
		//4. 분석 : 최고 가격, 최고가격을 뺸 해당제품의 총합
		//	1)최고 가격 -> Max값 찾기, 해당 제품을 제외한 (조건문)합을 구함 
		//  Product pd = null;
		//	2-1)최고 가격의 제품 찾을 떄 인덱스 또는 값을 따로 저장함
		//	for(int i = 0; i.pd.length; i++)
		//	2-2)반복문을 한번 더 돌려서 해당 제품을 제외하고 (조건문)합을 구함
		
		//5. 종료시 프로그램이 종료한다는 메세지를 출력
		//	System.out.println 활용
		
		//클래스 설계 => Product
		
		
		//상품수와 상품명을 입렵 받을 수 있도록  Scanner 등록
		//어디서든 사용할 수 있도록 배열 밖에서 스캐너를 만들어줌
		Scanner sc = new Scanner(System.in);
		Product99[] pd = null; 
		int productCount = 0; //배열이 한바퀴 돌고 다시 돌아왔을 때 초기화되는 것을 막기 위해  while문 밖에 만듦
		
		while(true) {
			
	
				//배열의 크기만 받는 행동만 함 2번에서 데이터를 넣을 때 배열의 크기를 확정
			System.out.println("1.상품수 | 2.상품 및 가격 | 3.제품별 가격 | 4.분석 | 5.종료");
			System.out.println("입력>");
				
				String selectNo = sc.nextLine();
				if(selectNo.equals("1")) {
					System.out.println("상품 수 입력>");
					productCount = Integer.parseInt(sc.nextLine());
				}else if(selectNo.equals("2")) {
					pd = new Product99[productCount];
					//상품 수 입력 받은 내뇽르 토대로 배열 크기를 확정
					//productCount = 5
					//pd.length = 5
					//i = 0 ~ 4
					for(int i =0; i<pd.length; i++) {
						//상품 객체 생성 - 반복문이 반복될 때 마다 초기화 시켜줌
						//새로운 방에 새로운 값을 넣어 주기 위해
						//반복문이 실행될 때 마다 새로운 상품을 만들기 위해
						Product99 product = new Product99();
						System.out.println((i+1)+"번째 상품");
						System.out.println("상품명>");
						//변수에 데이터를 입력 받고 객체에 데이터를 넣는 방법
						String name= sc.nextLine();
						product.ProductName = name;
						
						
						//데이터를 입력 받음과 동시에 객체에 데이터를 넣는 방법
						System.out.println("가격>");
						product.price = Integer.parseInt(sc.nextLine());
						
						pd[i] = product;
						System.out.println("==============================");
					}
				}else if (selectNo.equals("3")) {
					//배열의 크기만 반복을 진행할 때 배열에 각 인덱스(방번호)를
					//활용하여 객체(상품)을 꺼내와서 객체 (상품)에 정보를 하나씩 꺼내옴
					for(int i = 0; i<pd.length; i++) {
						String name = pd[i].ProductName;
						//String name = product.ProductName;
						//상품가격
						int price = pd[i].price;
						
						System.out.println("상품명 : " + name);
						System.out.println("상품 가격 :" + pd[i].price);
					}
				}else if(selectNo.equals("4")) {
					int max = pd[0].price;
					int sum = 0;
					for(int i =0; i<pd.length; i++) {
						if(max < pd[i].price) {
							max = pd[i].price;
						}
						sum += pd[i].price;
					}
					System.out.println("최고가상품 : "+ max);
					System.out.println("최고가를 뺸 상품의 총합 : " +(sum-max));
					
				}else if(selectNo.equals("5")) {
					System.out.println("종료");
					break;
				}
				
			}
		}//while
		
		
		
		
		
		
		
		
		
		
		
	}

