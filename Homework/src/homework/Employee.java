package homework;

public class Employee {

	//필드
	String name;
	int salary;
	//생성자 
	//(필드를 생성자를 통해 초기화 => 생성자의 매개변수 필요)
	public Employee(String name, int salary){
		this.name = name;
		this.salary = salary;
	}
	
	//메소드
	//getter를 통해 데이터를 가져와서 사용할 수 있음
	public String getName() {
		return name;
	}
	
	public int getSalary() {
		return salary;
	}

	public void getInformation() {
		System.out.println("이름"+name);
		System.out.println("연봉"+salary);
	}
	public void print() {
		System.out.println("슈퍼클래스");
	}
	
}
