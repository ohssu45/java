package homework;

public class EmployeeApplication {

	public static void main(String[] args) {
		//부모생성자에 맞게 내용을 넣어 주어야 함
		Employee employee = new EmpDept("이지나", 3000, "교육부");
		EmpDept empdept =(EmpDept) employee;
		
		System.out.println("이름:" + empdept.name);
		System.out.println("연봉:" + empdept.salary);
		System.out.println("부서:" + empdept.department);
		
		empdept.getName();
		empdept.getSalary();
		empdept.getDepartment();
		

	}

}
