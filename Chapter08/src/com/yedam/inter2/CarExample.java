package com.yedam.inter2;

public class CarExample {
	public static void main(String[] args) {
		Car myCar = new Car();
		
		myCar.run();
		
		myCar.fontLeftTire = new KumhoTire();
		myCar.backRightTire = new HankookTire();
	}
}
