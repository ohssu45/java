package com.yedam.inter2;

public class ImplementC implements InterfaceC {

	@Override
	public void methodA() {
		System.out.println("methodA");
		
	}

	@Override
	public void methodB() {
		System.out.println("methodB");
		
	}

	@Override
	public void methodC() {
		System.out.println("methodC");
		
	}

}
