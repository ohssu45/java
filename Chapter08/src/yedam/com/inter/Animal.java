package yedam.com.inter;

public interface Animal {
	
	void walk();
	void fly();
	void sing();
}
