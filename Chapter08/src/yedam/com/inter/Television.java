package yedam.com.inter;

public class Television implements RemoteControl {
	//필드
	private int volume;
	
	//생성자
	
	//메소드
	@Override
	public void turnOn() {
	System.out.println("전원을 켭니다.");	
		
	}

	@Override
	public void turnOff() {
		System.out.println("전원을 끕니다.");	
		
	}

	@Override
	public void setVolume(int volume) {
		if(volume > RemoteControl.MAX_VOLUME) {
			//최대볼륨보다 더 많은 데이터가 들어올 경우
			this.volume = RemoteControl.MAX_VOLUME;
		}else if(volume < RemoteControl.MIN_VOLUME) {
			//최소볼륨보다 더 적은 데이터가 들어올 경우
			this.volume = RemoteControl.MIN_VOLUME;
		}else {
			this.volume = volume;
		}
		System.out.println("현재 tv 볼륨 :" + this.volume);
	}

}
