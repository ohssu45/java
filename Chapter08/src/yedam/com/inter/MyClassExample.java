package yedam.com.inter;

public class MyClassExample {
	public static void main(String[] args) {
		
	
	System.out.println("1)========================");
	MyClass myClass = new MyClass();
	//필드 안의 객체(rc)를 부르고 객체가 가지고 있는 기능 호출
	myClass.rc.turnOn();
	myClass.rc.turnOff();
	
	System.out.println();
	System.out.println("2)========================");
	
	MyClass myClass2 = new MyClass(new Audio());
	
	System.out.println("3)========================");
	MyClass myClass3 = new MyClass();
	myClass3.method1();
	
	System.out.println("4)========================");
	
	MyClass myClass4 = new MyClass();
	myClass4.methodB(new Television());
	}
}
