package yedam.com.inter;

public interface WashingMachine extends DryCourse{
	public void startBtn();
	public void pauseBTn();
	public void stopBTn();
	public int changeSpeed(int speed);
}
