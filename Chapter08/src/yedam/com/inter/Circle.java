package yedam.com.inter;

public class Circle implements GetInfo {
	//필드
	int radius;
	//생성자
	public Circle(int radius) {
		this.radius = radius;
	}
	//메소드
	
	
	@Override
	public void area() {
		//원넓이 PI * R* R
		System.out.println( Math.PI * radius * radius);
	}

	@Override
	public void round() {
		//원둘레 2 * PI * R
		System.out.println(2 * Math.PI * radius);
	}


}


