package yedam.com.inter;

public class LGWashingMachine implements WashingMachine{

	@Override
	public void dry() {
		System.out.println("건조코스진행.");
		
	}

	@Override
	public void startBtn() {
		System.out.println("빨래 시작.");
		
	}

	@Override
	public void pauseBTn() {
		System.out.println("빨래 일시 중지");
		
	}

	@Override
	public void stopBTn() {
		System.out.println("빨래중지");
		
	}

	@Override
	public int changeSpeed(int speed) {
		int nowSpeed = 0;
		switch(speed) {
		case 1:
			nowSpeed = 20;
			break;
		case 2:
			nowSpeed = 50;
			break;
		case 3:
			nowSpeed = 70;
			break;
		case 4:
			nowSpeed = 90;
			break;
		}
		return 0;
	}
	
}
