package com.yedam.array;

public class Ex08 {
	public static void main(String[] args) {
		String[] strArray = new String[3];
		
		//strArray[0] ="yedam"; 밑의 두줄과 같은 식
		String str = "yedam"; //주소
		strArray[0] = str;
		//strArray[0] == str == "yedam";
		strArray[1] ="yedam";
		
		strArray[2] = new String("yedam");
		
		System.out.println(strArray[0] == strArray[1]); //주소비교
		System.out.println(strArray[0] == strArray[2]); //주소비교
		System.out.println(strArray[0].equals(strArray[2])); // 데이터 비교
	
	//============================================
		//for문을 활용한 배열 복사
		int[] oldArray = {1,2,3};
		
		int[] newArray = new int[5];
		
		for(int i =0; i<oldArray.length; i++) {
			newArray[i] = oldArray[i];
		}
		for(int i=0; i<newArray.length; i++) {
			System.out.println(newArray[i]);
		}//12300 방이 다섯개이기 때문에 남은 공간은 0이 출력
		
	//=============================================
		//system.Arraycopy 사용하여 배열복사
		int[] oldArray2 = {1,2,3,4,4,5,6,7};
		
		int[] newArray2 = new int[10];
		
		System.arraycopy(oldArray2, 0, newArray2, 0, oldArray2.length);
		
		for(int i =0; i<newArray2.length; i++) {
			System.out.println(newArray2[i]);
		}
	//===============================================
		//
		for(int temp : newArray2) {
			System.out.print(temp+"\t");
		}//temp=newArray2 \t=tap
	
	}
}
