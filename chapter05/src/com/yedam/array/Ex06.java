package com.yedam.array;

public class Ex06 {
	public static void main(String[] args) {
		//최댓값의 인덱스 구하기(최댓값이 존재하는 방 번호 구하기)
		int[] array = {10,50,70,20,30,80,40};

		int maxIndex = 0;
		int max = array[0];
		for(int i= 0; i<array.length; i++) {
			if(max < array[i]) {
				max = array[i];
				maxIndex=i;
			}
		}
		System.out.println("최댓값 : " + max);
		System.out.println("최댓값의 위치 : " + maxIndex);
		
	}
}
