package com.yedam.array;

import java.util.Scanner;

public class Ex03 {
	public static void main(String[] args) {
		//배열의 크기를 가변적으로 만듦
		Scanner sc = new Scanner(System.in);
		
		int[] ary;
		int no;
		
		System.out.println("배열의 크기>");
		no = Integer.parseInt(sc.nextLine());
		
		ary = new int[no];
		System.out.println(ary.length);
		
		//내가 입력하는 값으로 배열의 크기를 정할 수 있음
		//단 딱 한번만 배열크기는 정할 수 있음 수정불가
		
		for(int i=0; i<ary.length; i++) {
			System.out.println("입력>");
			ary[i] = Integer.parseInt(sc.nextLine());
		} //내가 입력한 배열의 크기만큼 값을 입력할 수 있음
		
		
		
		
		
		
		
	}
}
