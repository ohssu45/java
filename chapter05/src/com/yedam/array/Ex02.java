package com.yedam.array;

import java.util.Scanner;

public class Ex02 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		//국영수의 데이터를 담는 배열
		int[] point = new int[3];
		
		System.out.println("국어점수>");
		point[0] = Integer.parseInt(sc.nextLine());
		
		System.out.println("영어점수>");
		point[1] = Integer.parseInt(sc.nextLine());
		
		System.out.println("수학점수>");
		point[2] = Integer.parseInt(sc.nextLine());
		//내가 원하는 값을 넣어 계산할 수 있음
		
		//array.length
		//1)점수의 총 합계
		int sum = 0; //데이터를 담을 수 있는 변수
		System.out.println("점수 배열의 크기 : " + point.length); //3 => 배열의 크기
		for(int i=0; i<point.length; i++) { //배열의 크기를 모를 때 배열의 크기만큼 반복문을 실행
			sum = sum + point[i]; //0~2의 방에 있는 값들을 가져와서 하나씩 더함
		}
		System.out.println("점수의 총 합계 : " + sum);
		
		//2)점수의 평균
		double avg =(double) sum / point.length; 
		//int/int이기 때문에 하나에 (double)을 넣어 계산
		 System.out.println("점수의 평균 : " + avg);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}//main
}
