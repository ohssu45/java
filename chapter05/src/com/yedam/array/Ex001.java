package com.yedam.array;

public class Ex001 {
	public static void main(String[] args) {
		int[] intArry = {1, 2, 3, 4, 5, 6};
		//int[] intArry = new int[6];
		//intAryy[0] = 1
		//intArry[1] = 2 ...
		
		String[] strArry = new String[10];
	
		int[] intArry2 ;
		intArry2 = new int[5];
	
		int[] scores = {83, 90, 87}; //배열과 동시에 값을 넣음
		
		System.out.println("scores 첫번째 값 : " +scores[0]);
		System.out.println("scores 세번째 값 : " +scores[2]);
	
		//반복문과 배열
		int sum = 0;
		
		for(int i =0; i<3; i++) { //방번호를 i에 대입
			System.out.println(scores[i]);
			sum = sum + scores[i]; //0~2번의 데이터를 꺼내 모두 합함
		}
		System.out.println("총 합계 :" + sum);
	
		//new연산자를 활용해서 배열 만들기
		int[] point; //정수만 넣을 수 있는 집
		point = new int[] {83, 90, 87}; //값 넣기, 방은 3개

		sum = 0;
		for(int i=9; i<3; i++) {
			System.out.println(point[i]);
			sum=sum+point[i];
		} 
		System.out.println("총 합계 : " + sum);
		
	//2)
		int[]arr1 = new int[3];
		for(int i=0; i<3; i++) {
			System.out.println("arr1["+i+"] : " + arr1[i]);
		} //기본값 0이 3개의 방에 들어가 있음
		arr1[0] = 10;
		arr1[1] = 20;
		arr1[2] = 30;
	
		for(int i=0; i<3; i++) {
			System.out.println("arr1["+i+"] : " + arr1[i]);
		}
	
		//실수형 타입의 배열
		double[] arr2 = new double[3];
		for(int i=0; i<3; i++) {
			System.out.println("arr2["+i+"] : " + arr2[i]);
		} //기본 값 0.0이 들어가 있음
		
		arr2[0] = 0.1;
		arr2[1] = 0.2;
		arr2[2] = 0.3;
		for(int i=0; i<3; i++) {
			System.out.println("arr2["+i+"] : " + arr2[i]);
		}
		
		//문자열
		String[] arr3 = new String[3];
		for(int i=0; i<3; i++) {
			System.out.println("arr3["+i+"] : " + arr3[i]);
		} //기본값 null이 나옴
		
		arr3[0] ="3월";
		arr3[1] ="11월";
		arr3[2] ="12월";
		for(int i=0; i<3; i++) {
			System.out.println("arr3["+i+"] : " + arr3[i]);
		}
		
		
		
		
		
		
		
		
	}//main
}

