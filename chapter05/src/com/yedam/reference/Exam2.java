package com.yedam.reference;


public class Exam2 {
	public static void main(String[] args) {
		String strVal1 = "yedam";
		String strVal2 = "yedam";
		
		//참조 타입의 ==는 데이터를 비교하는게 아니고 데이터가 있는 메모리 주소를 
		//비교하는 것
		if(strVal1 == strVal2) {
			System.out.println("StrVal1과 strVal2는 메모리 주소 같다.");
		}else {
			System.out.println("strVal1과 strVal2는 메모리 주소 다르다");
		} //StrVal1과 strVAl2는 메모리 주소 같다
		
		if(strVal1.equals(strVal2)) {
			System.out.println("strVal1과 strVal2의 데이터는 같습니다.");
		}else {
			System.out.println("strVal1과 strVal2의 데이터는 다릅니다.");
		} // strVal1과 strVal2의 데이터는 같습니다.
		
		
		
		String strVal3 = new String("yedam");
		String strVal4 = new String("yedam");
		
		if(strVal3 == strVal4) {
			System.out.println("StrVal3과 strVal4는 메모리 주소 같다.");
		} else {
			System.out.println("strVal3과 strVal4는 메모리 주소 다르다");
		}//strVal3과 strVal4는 메모리 주소 다르다
		
		if(strVal3.equals(strVal4)) {
			System.out.println("strVal3과 strVal4의 데이터는 같습니다.");
		}else {
			System.out.println("strVal3과 strVal4의 데이터는 다릅니다.");
		}//strVal3과 strVal4의 데이터는 같습니다.
		
		if(strVal1 == strVal3) {
			System.out.println("strVal1과 strVal3은 같은 메모리 주소를 가지고 있다.");
		} else {
			System.out.println("strVal1과 strVal3은 다른 메모리 주소를 가지고 있다.");
		} //"strVal1과 strVal3은 다른 메모리 주소를 가지고 있다."
		
	}
}
