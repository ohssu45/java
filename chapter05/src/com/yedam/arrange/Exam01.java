package com.yedam.arrange;

import java.util.Calendar;

public class Exam01 {
	public static void main(String[] args) {
		Week  today = null; //Week란 열거형식의 변수 데이터를 사용하겠습니다 ->어떤 값을 사용하지 모르기 때문에 null
		
		Calendar cal = Calendar.getInstance();
		int week = cal.get(Calendar.DAY_OF_WEEK); //열거형식의 변수를 사용
		//할 수 있게 데이터를 받아옴
		switch(week) {
		case 1:
			today = Week.SUNDAY;
			break;
		case 2:
			today = Week.MONDAY;
			break;
		case 3:
			today = Week.TUESDAY;
			break;
		case 4:
			today = Week.WEDNESDAY;
			break;
		case 5:
			today = Week.THURSDAY;
			break;
		case 6:
			today = Week.FRIDAY;
			break;
		case 7:
			today = Week.SATURDAY;
			break;
		
		}
		System.out.println("오늘 요일 : " +today);
	}
}
