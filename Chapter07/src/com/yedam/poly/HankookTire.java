package com.yedam.poly;

public class HankookTire extends Tire {
	//자식 클래스
	//생성자
	public HankookTire(String location, int maxRotation) {
		super(location, maxRotation);
	}
	//메서드
	@Override
	public boolean roll() {
		++accRotation;
		if(accRotation < maxRotation) {
			System.out.println(location + "Han Tire 수명 :" +
		(maxRotation-accRotation) + "회");
			return true;
		} else {
			System.out.println("###" + location + "Han Tire 펑크" + "###");
			return false;
		}
	}

	
}
