package com.yedam.poly;

public class DrawExample {

	public static void main(String[] args) {
		// 자동타입변환 + override
		//부모클래스 변수 = new 자식 클래스
		
		Draw circle = new Circle();
		circle.x =1;
		circle.y =2;
		circle.draw();
		
		System.out.println();
		
		circle = new Rectangle(); //다른 클래스로 초기화
		circle.draw();
	}

}
