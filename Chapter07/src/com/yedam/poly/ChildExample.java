package com.yedam.poly;

public class ChildExample {
	public static void main(String[] args) {
		//Child child = new Child();
		
		//☆★클래스 간의 자동타입변환★☆
		//부모 클래스에 있는 메소드를 사용하되, 자식을 참조
		//단, 자식클래스에 재정의가 되어 있으면
		//자식 클래스의 재정의된 메소드를 사용함
		//Parent parent = child;
		
		//parent.method1();
		//parent.method2();
		//parent.method3();//자식클래스에만 사용된 메소드는 사용 불가
		
		//========================================
		//클래스 간의 강제 타입 변환
		//자동타입변환으로 인해서 자식클래스 내부에 정의된 필드, 메소드를 못 쓸 경우
		//강제타입변환을 함으로써 자식 클래스 내부네 정의된 필드와 메소드를 사용.
		
		Parent parent = new Child();
		
		parent.field1 = "date1";
		parent.method1();
		parent.method2();
		
		//자식 클래스가 가지고 있는 메소드 호출
//		parent.field2 = "data2";
//		parent.method3();
		
		// 강제 타입 변환
		Child child = (Child) parent;
		child.method3();
		child.field2 = "data2";
		child.field1 = "data";
		child.method1();
		child.method2();
		
		//=======================================
		//클래스 타입 확인 예제
		method1(new Parent()); //변환실패 자기 자신으로 만들어진 객체- 다른 클래스로 인식 실패
		method1(new Child());//변환 성공 자동타입변환,
		
		
		GrandParent gp = new Child();
		gp.method4();
		
		
		
	}//main
	//정적메소드로 만들어줌 
	//자식 클래스로 자동변환 된 게 들어옴?
	public static void method1(Parent parent) {
		if(parent instanceof Child) {
			Child child = (Child) parent; //parent 강제 타입 변환
			System.out.println("변환 성공");
		}else {
			System.out.println("변환 실패");
		}
	}
}
