package com.yedam.ingeri;

public class OverrideExam {
	public static void main(String[] args) {
		Child child = new Child();
		
		child.method1();
		//자식
		child.method2();
		//부모
		child.method3();
		//자식
	}
}
