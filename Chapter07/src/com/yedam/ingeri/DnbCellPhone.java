package com.yedam.ingeri;

public class DnbCellPhone extends CellPhone {

	//필드
	int channel;
	
	//생성자
	public DnbCellPhone(String model, String color, int channel) {
		this.model = model;
		this.color = color;
		this.channel = channel;
	}
	//메소드
		void trunOnDnb() {
			System.out.println("채널" + channel + "번 방송 수신");
		}
		
		void trunOffDnb() {
			System.out.println("방송을 멈춤");
		}
}
