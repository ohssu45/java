package com.yedam.ingeri;

public class Dnb {
	public static void main(String[] args) {
		DnbCellPhone dnb = new DnbCellPhone("자바폰", "검정", 10);
		//부모클래스 필드 호출
		System.out.println(dnb.model);
		System.out.println(dnb.color);
		//자식 클래스 필드 호출
		System.out.println(dnb.channel);
		
		//부모 클래스의 메소드 호출
		dnb.powerOn();
		dnb.bell();
		dnb.hangUp();
		
		//자식 클래스 메소드 호출
		dnb.trunOnDnb();
		dnb.trunOffDnb();
		
		dnb.poserOff();
		
	}
}
