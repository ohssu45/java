package com.yedam.ingeri;

public class Application {
	public static void main(String[] args) {
		Child child = new Child();
		
		//child에 정의 해놓은 필드
		child.lastName = "또치";
		child.age = 20;
		
		//child에서 first name을 따로 지정하지 않았지만 바로 사용할 수 있음
		System.out.println("내 이름 : " + child.firstName + child.lastName);
		System.out.println("DNA : " + child.DNA);
		//Parent 클래스 -> bloodType을 private 설정함
		//Child  클래스 -> Parent 클래스에서 사용할 수 없기 때문에 오류 발생
		//System.out.println("혈액형 : " + child.bloodType);
		//age는 child에서 정의 한 값을 가져옴
		System.out.println("나이 : " +child.age);
		
		//==========================================
		Child2 child2 = new Child2();
		
		child2.lastName = "희동";
		child2.age = 5;
		child2.bloodType = 'A';
		
		System.out.println();
		System.out.println("내 이름 : " + child2.firstName + child2.lastName);
		System.out.println("DNA : " + child2.DNA);
		//Child 클래스에서 설정하였기 때문에 오류가 나지 않음
		System.out.println("혈액형 : " + child2.bloodType);
		System.out.println("나이 : " +child2.age);
		
		
	}
}
