package com.yedam.ingeri;

public class AirPlaneExample {
	public static void main(String[] args) {
		SuperSoniAirPlane sa = new SuperSoniAirPlane();
		
		sa.takeOff(); //부모
		
		sa.fly(); //자식클래스에서 오버라이딩 초기값 => NORMAR
		
		sa.flyMode = SuperSoniAirPlane.SUPERSONIC; //flyMode 수정
		
		sa.fly(); //if문의 조건식을 만족했기 때문에 조건문 실행
		
		sa.flyMode = SuperSoniAirPlane.NORMAR; 
		
		//if문의 조건식을 만족하지 못했기 때문에 else문 실행 => 부모 메소드 실행 => NORMAR
		sa.fly();
		
		sa.land();
	}
}
