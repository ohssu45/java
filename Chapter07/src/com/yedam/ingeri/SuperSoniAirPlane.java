package com.yedam.ingeri;

public class SuperSoniAirPlane extends AirPlane {
	//필드
	public static final int NORMAR = 1;
	public static final int SUPERSONIC = 2;
	//생성자
	public int flyMode = NORMAR;
	
	
	
	
	//메소드
	@Override
	public void fly() {
		if(flyMode == SUPERSONIC){
			System.out.println("초음속 비행 모드");
		} else {
			super.fly();
		}
	
	}
	
}
