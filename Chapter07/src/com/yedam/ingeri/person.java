package com.yedam.ingeri;

public class person extends People {
//people 상속
	//기본생성자를 사용할려고 했지만 부모에 생성자가 만들어져 있기 때문에
	//자식은 부모를 대신해 정보를 받아와야 함 => super()
	//자식객체를 만들 때 부모객체도 함께 만듦 생성자 2개 
	public int age;
	//자식 객체를 만들 때, 생성자를 통해서 만듦
	//super()를 통해서 부모 객체를 생성
	//여기서 super() 의미하는 것은 부모의 생성자를 호출
	//따라서 자식 객체를 만들게 되면, 부모객체도 같이 만들어짐
	public person(String name, String ssn) { 
		super(name, ssn);
	}
}
