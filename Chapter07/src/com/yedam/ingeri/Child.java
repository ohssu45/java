package com.yedam.ingeri;

public class Child extends Parent {
	//자식 => extends -> 부모와 연결시켜줌
	public String lastName;
	public int age;
	
	//========================
	//메소드 오버라이딩 예제
	
	@Override //자바에 알려줌
	//오버라이드 예제
	protected void method1() {
		System.out.println("child class -> method1 Override");
	}
	//protected, public만 가능
	public void method3() {
		System.out.println("child class -> method3 Override");
	}
	@Override //source -> override에서 사용 가능 타이핑 안쳐도 됨
	public void method2() {
		// TODO Auto-generated method stub
		super.method2();
	}
	
	
}

