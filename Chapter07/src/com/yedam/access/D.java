package com.yedam.access;

import com.yedam.ingeri.A;

public class D extends A{
	//다른 패키지의 부모생성자를 호출하고 필드, 메소드에 접근 가능
	public D() {
		//부모생성자 호출 + 객체생성
		super();
		//부모 필드 접근
		this.field = "value";
		//부모 메소드 접근
		this.method();
	}
}
