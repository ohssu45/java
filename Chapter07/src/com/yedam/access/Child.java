package com.yedam.access;

import com.yedam.ingeri.Parent;

public class Child extends Parent {
	//같은 패키지가 이니기 때문에 오류발생  -> import 사용 부모의 경로 설정해줌
	public String lastname;
	public int age;
	
	public Child() {
	}
	
	//서로 다른 패키지에 존재하는 부모의 필드를 가져와 사용 가능
	public void showInfo() {
		System.out.println("내 성씨 : " + firstName);
		System.out.println("DNA" + DNA);
	}
	
}
