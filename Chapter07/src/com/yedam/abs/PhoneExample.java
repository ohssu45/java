package com.yedam.abs;

public class PhoneExample {
	public static void main(String[] args) {
	//추상 클래스 객체(인스턴스)화 확인
	//Phone phone = new Phone("주인");
	
	SmartPhone smartPhone = new SmartPhone("홍길동");
	smartPhone.turnOff();
	smartPhone.turnOn();
	
	smartPhone.internetSearch();
	
	
	}
}
