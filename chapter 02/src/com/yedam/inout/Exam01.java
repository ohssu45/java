package com.yedam.inout;

import java.io.IOException;
import java.util.Scanner;

public class Exam01 {
	public static void main(String[] args) throws Exception {
		int value = 123;
		String name = "상품";
		double price = 1000.10;
		System.out.printf("상품의 가격 : %d\n", value); //()안에 내용이 없을 시 오류
		System.out.printf("%s의 가격 : %d\n", name, value);
		System.out.printf("%s의 가격 : %d원, %f\n", name, value, price);
		
		//1) 정수 사용
		value = 11;
		System.out.printf("%d\n", value);
		System.out.printf("%6d\n", value);
		System.out.printf("%-6d\n", value);
		System.out.printf("%06d\n", value);
		
		//2) 실수 사용
		price = 321.4567;
		System.out.printf("%f\n", price);
		System.out.printf("%10.2f\n", price);//반올림ㅇ123.46
		System.out.printf("%-10.2f\n", price);
		System.out.printf("%010.2f\n", price);
		System.out.printf("%f\n",(double)value/5);
		
		//3)문자열 사용
		System.out.printf("%s\n", "문자열사용");
		System.out.printf("%6s\n", "문자열사용");
		System.out.printf("%-6s\n", "문자열사용");
		
		//4)기본 출력물
		System.out.print("아무것도 없는 print");
		
		//입력
		
//		int keyCode;
//		
//		keyCode = System.in.read("원하는 값 입력>");
//		System.out.println("keyCode : " + keyCode);
//		
		
		//스캐너(Scanner)-통암기
		Scanner sc = new Scanner(System.in); //sc=변수, 초기화
		System.out.println("데이터 입력>"); //console에 알려줌
		String word = sc.nextLine(); //nextLine() : 데이터를 입력하겠습니다. =>문자열 입력
		System.out.println(word); 
		//next : 사용시 엔터키는 남고 데이터만 가져감
		//next와 nextLine 혼용시 nextLine이 엔터 옆의 빈여백을 데이터라 생각하고 빈여백을 출력시킴
		//nextLine : 엔터키와 여백 둘다 가져감

		//문자열의 데이터 비교
		if(word.equals("test")) { 
			System.out.println("equal: 입력하신 문자열과 비교 문자열이 같음.");
		}
		//정수형, 실수형, boolean 비교, 문자열은 안됨
		if(word == "test") { 
			System.out.println("==: 입력하신 문자열과 비교 문자열이 같음.");
		}
		
		//문자열 비교는  == 아니고 문자열.equals(비교대상)
		
		//논리 연산자 => && <-> &, || <-> |
		//&& => 단 하나의 조건이 F => F (F && T && F && T)
		//							 ( 0 * 1 * 0 * 1)  F를 인식하면 그 뒤 연산식을 실행하지 않음
		//							& => 4가지 연산을 다 함
		// || => 단 하나의 조건이 t => t (F || T || F || T)
		//0*1 => 0 
		//
		
		int charCode = 'A';
		//'A'~'Z'
		if(charCode>=65 && charCode <=90) {
			System.out.println("대문자.");
		}
		//48~57
		//charCode >= 48 && charCode <= 57
		// !< --> >=, !> --> <=
		if(!(charCode<48) && !(charCode>57)) {
			System.out.println("0~9 숫자.");
		}
		
		int value2 = 6;
		if(value2 %2==0 || value2 %3==0) {
			System.out.println("2 또는 3의 배수");
		}
		
		
		//대입 연산자 =, +=, -+, ....
		
		int result5 = 0;
		result5 += 1; //result5 = result5+1
		result5 -= 1;
		result5 *= 3;
		result5 /= 10;
		
		//삼항 연산자
		
		int score = 85;
		
		char grade=(score>90)? 'A' : 'B';
		//(조건)이 참일 경우 -> A, 거짓일 경우 -> B 그 값이 grade의 자리로 들어감
		char grade3 = (score>90)? 'A' : ((score>80)? 'B' : 'c');
		char grade2;
		
		if(score > 90) {
			grade2 = 'A';
		} else {
			grade2 = 'B';
		}
		
		
		
		
		
		
		
		
	}
}
