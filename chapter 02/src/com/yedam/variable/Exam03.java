package com.yedam.variable;

public class Exam03 {
	public static void main(String[] args) {
		//자동 타입 변환
		char cVar = 'A'+1; //65
		int iVar = cVar;
		System.out.println(iVar); //66
		double dVar = iVar;
		System.out.println(dVar); //66.0
		
		//강제 타입 변환(casting)
		iVar = (int)dVar;
		System.out.println(iVar);
		cVar = (char)iVar;
		System.out.println(cVar);
		
		double dVar2 = 3.1444444;
		int iVar2 = (int)dVar2;
		System.out.println(iVar2); //3 (정수의 성질을 활용, 소수점 아래 값은 출력되지 않음)
		
		byte result = 10 + 20;
		System.out.println(result);
		
		byte x = 10;
		byte y = 20;
		int result2 = x+y;
		//강제 타입 변환 활용
		byte result3 = (byte)(x+y);
		//데이터 타입 크기에 따른 
		//long + int = long
		//byte + int = int
		byte bVar =10;
		int iVar1 = 100;
		long lVar = 1000L;
		long result4 =(int)(bVar+ iVar+lVar);
		System.out.println(result4);
		
		
		
	}
}
