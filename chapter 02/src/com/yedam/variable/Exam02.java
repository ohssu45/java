package com.yedam.variable;

public class Exam02 {
	public static void main(String[] args) {
		int var1 = 0b1011; //2진수
		int var2 = 0206; //8진수
		int var3 = 365; //10진수
		int var4 = 0xB3; //16진수
		
		System.out.println(var1);
		System.out.println(var2);
		System.out.println(var3);
		System.out.println(var4);

		//byte -128 ~ 127
		byte bval = -128;
		byte bval2 = 0;
		byte bval3 = 127;
		int bval4 = 500;
		
		//long
		
		long lval = 10;
		long lval2 = 20L;
		long lval3 = 10000000000L; //int 타입의 범의를 벗어날 때는 L을 붙여야함
		
		
		//char (유니코드)
		//문자만 넣을 수 있음
		//문자열과 문자는 다름
		//문자열(string) => 문자가 모여서 만들어 진 것.
		//ex) "apple"
		//문자 => 하나의 알파벳
		//ex) 'A', 'B'
		
		char cVar = 'A';
		char cVar2 = '가';
		char cVar3 = 67; //C
		char cVar4 = 0x0041; //A
		
	System.out.println(cVar);
	System.out.println(cVar2);
	System.out.println(cVar3);
	System.out.println(cVar4);
	
	//문자열 ""
	//char cVal5 = "홍길동";
	//String str1 = '홍길동';
	
	String str = "홍길동";
	String str2 = "프로그래머";
	System.out.println(str2);
	System.out.println(str);
	
	//이스케이프 문자
	
	System.out.println("번호\t이름\t직업");
	//탭만큼 띄움
	System.out.println("행 단위 출력\n");
	//엔터
	System.out.println("우리는 \"개발자\" 입니다.");
	//특수문자 존재 여부 확인 \"개발자\" : 출력문(console) => "개발자"
	System.out.println("봄\\여름\\가을\\겨울");
	//특수문자 존재 여부 확인 \ : 출력문 => \
	
	//실수 타입
	//float
	float fVal = 3.14f; //리터럴에 f를 넣어 타입을 확실하게 정의해줘야함 
	//double
	double dVal = 3.14;
	
	//e 사용하기
	double dVal2 = 3e6;
	float fVal2 = 3e6f;
	double dVal3 = 2e-3;
	//double temp11 = 50000000 -10; //E=10
	//System.out.println(temp11);
	System.out.println(dVal2); //e = 제곱근 3 * 10의 3승
	System.out.println(fVal2); // 3 * 10의 6승
	System.out.println(dVal3); // 3 * 10의 -3승 => 2/1000
	
	//논리타입 -true false
	
	boolean stop = true;
	
	if(stop) {
		System.out.println("중지합니다.");
	} else {
		System.out.println("시작합니다.");
	} //중지합니다. 
	
	
	
	
	
	
	
	}
}
