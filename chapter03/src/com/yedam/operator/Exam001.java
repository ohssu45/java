package com.yedam.operator;

public class Exam001 {
	public static void main(String[] args) {
		//부호연산자 +,-
		int x = -100;
		int result1 = -x;
		int result2 = +x;
		System.out.println("result1 : " + result1);
		System.out.println("result2 : " + result2);
		
		byte b = 100;
		//byte result3 = -b; 오류 부호를 붙이는 순간 int 형변환
		int result3 = -b;
		
		//증감 연산자 ++, --
		//위치에 따라서 연산 타이밍이 다름
		int value = 100;
		
		//출력물이 실행되고 나서 value 값 1증가
		System.out.println(value++); //console창에 출력되고 난 후 값 1증가
		
		//출력문이 실행되기 전 value 값 1증가
		System.out.println(++value); //101에서 +1 하고 난 후 console창에 출력
		
		//논리 부정 연산자
		//true + not = false
		//false + not = true
		boolean flag = false; //flag : 기준점
		if(!flag) { //()안의 명제가 true여야 밑의 조건식이 진행됨
			System.out.println("false");
		}
		if(flag) {
			System.out.println("true");
		}
		
		
		//이항연산자(사칙연산)
		int v1 = 10;
		int v2 = 4;
		
		System.out.println(v1 + v2);
		System.out.println(v1-v2);
		System.out.println(v1*v2);
		
		//나누기 = 2개
		// 몫을 구하는 연산자 = /
		// 나머지를 구하는 연산자 = %
		
		System.out.println("/ : "+(v1/v2));
		System.out.println("% : "+(v1%v2));
		
		//문자열 결합 연산자(+), 딱풀 연산자
		System.out.println("자바" + "jdk" + "11버전");
		
		//비교연산자
		
		int num1 = 10;
		int num2 = 10;
		
		System.out.println(num1 == num2);
		System.out.println(num1 != num2);
		System.out.println(num1 >= num2);
		
		char char1 = 'A';
		char char2 = 'B';
		
		System.out.println(char1<char2);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}
